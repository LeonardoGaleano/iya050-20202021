import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Principal from "./pages/principal.js";
import Login from "./pages/login.js";
import Registro from "./pages/registro.js";
import './assets/Styles/main.scss'

export default function App() {
  return (
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/principal">
            <Principal />
          </Route>
          <Route path="/registro">
            <Registro />
          </Route>
          <Route exact path="/">
            <Login />
          </Route>
          
        </Switch>
      </div>
    </Router>
  );
}