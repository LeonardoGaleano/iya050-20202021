import React from 'react';
import {useState} from 'react';
import { useHistory } from 'react-router';

function Registro() {
  const history = useHistory();
  const [usuario, setUsuario] = useState('');
  const ManejadorUsuario = (var1) => {
    let valorUsuario = var1.target.value
    setUsuario(valorUsuario)
  }
  const [nombrePila, setNombrePila] = useState('');
  const ManejadorNombrePila = (var2) => {
    let valorNombrePila = var2.target.value
    setNombrePila(valorNombrePila)
  }
  const [contraseña, setContraseña] = useState('');
  const ManejadorContra = (var3) => {
    let valorContra = var3.target.value
    setContraseña(valorContra)
  }

  const Authregistro = () => {
    fetch(
        "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + usuario,
        {
            method: "POST",
            body: JSON.stringify({
                firstName: nombrePila,
                password: contraseña,
            })
        }
    ).then(r => r.json()).then(r => {
      if (r.hasOwnProperty("username")) {
          history.push("/");
      }

    })
}
  return (
    <div className="d-flex justify-content-center">
        <div className="col-4 content borders">
            <h2 className="item">Registro</h2>
            <div className="item">
              <input type="text" placeholder="Nombre de usuario" className="textbox borders" onChange={ManejadorUsuario}/>
            </div>
            <div className="item">
              <input type="text" placeholder="Nombre de pila" className="textbox borders" onChange={ManejadorNombrePila}/>
            </div>
            <div className="item">
              <input type="password" placeholder="Contraseña" className="textbox borders" onChange={ManejadorContra}/>
            </div>
            <div className="item">
              <button className="btn buttons borders" onClick={Authregistro}>Realizar Registro</button>
            </div>
        </div>
    </div>
  )
}

export default Registro;