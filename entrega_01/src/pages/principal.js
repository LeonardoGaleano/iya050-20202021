import React from 'react';
import {useEffect, useState} from 'react';
import { useHistory } from 'react-router';
import '../assets/Styles/game.scss'
import {start} from '../assets/game'


function Principal() {
  const history = useHistory();
  const [datos, setDatos] = useState({}); 
  useEffect(() => {
    fetch(
      "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user/",
      {
        method: "GET",
        headers: {
            Authorization: window.localStorage.getItem("sesion"),
        },
        mode: 'cors'
      }
    ).then(r => r.json()).then(r => {
          if (r.hasOwnProperty("username")) {
            setDatos(r)
            start()
          } else{
            history.push("/");
          }
      })
}, [])

const Logout = () => {
  fetch(
    "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/" + window.localStorage.getItem("sesion"),
    {
      method: "DELETE",
      headers: {
          Authorization: window.localStorage.getItem("sesion"),
      },
    }
  ).then(r => {
        if (r.status===204) {
          history.push("/");
        }
    })
}

  return (
    <div className="d-flex justify-content-center">
        <div className="col-6 content">
          <div className="d-flex justify-content-between">
            <h2 className="item">Bienvenido {datos.firstName}</h2>
            <button className="btn btn-logout borders" onClick={Logout}>Cerrar Sesión</button>
          </div>
            <div>
            <div align="center">
			<div className="TableroJuego">
				<div id="iniciar" className="textoestatico instrucciones">Presiona espacio para empezar<br/>J1: W y S<br/>J2: O y L</div>
				<div className="pelota" id="pelota"></div>
				<div className="raqueta" id="player1"></div>
				<div className="raqueta" id="player2"></div>
				<div className="linea"></div>
			</div>
			<table className="info">
				<tr>
					<td  align="center"><span className="textoestatico">Jugador 1</span></td>
					<td  align="center"><span className="textoestatico">Jugador 2</span></td>
				</tr>
				<tr>
					<td align="center"><span className="textoestatico" id="Player1Points">0</span></td>
					<td  align="center"><span  className="textoestatico" id="Player2Points">0</span></td>
				</tr>
			</table>
		</div>
            </div>
        </div>
    </div>
  )
}

export default Principal;