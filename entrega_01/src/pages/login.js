import React from 'react';
import {useState} from 'react';
import { useHistory } from 'react-router';

function Login() {

  const history = useHistory();
  const [usuario, setUsuario] = useState('');
  const ManejadorUsuario = (var1) => {
    let valorUsuario = var1.target.value
    setUsuario(valorUsuario)
  }
  const [contraseña, setContraseña] = useState('');
  const ManejadorContra = (var2) => {
    let valorContra = var2.target.value
    setContraseña(valorContra)
  }

  const Manejador = () => {
    history.push("/registro")
  }


  const Authlogin = () => {
    fetch(
        "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + usuario + "/sessions",
        {
            method: "POST",
            body: JSON.stringify({
                password: contraseña,
            })
        }
    ).then(r => r.json()).then(r => {
      if (r.hasOwnProperty("sessionToken")) {
          window.localStorage.setItem("sesion", r.sessionToken)
          history.push("/principal");
      }

    })
  }

  return (
    <div className="d-flex justify-content-center">
    <div className="col-4 content borders">
        <h2 className="item">Login</h2>
        <div className="item">
          <input type="text" placeholder="Nombre de usuario" className="textbox borders" onChange={ManejadorUsuario}/>
        </div>
        <div className="item">
          <input type="password" placeholder="Contraseña" className="textbox borders" onChange={ManejadorContra}/>
        </div>
        <div className="item">
          <button className="btn butttons borders" onClick={Authlogin}>Ingresar</button>
        </div>
        <div className="item">
          <button className="btn butttons borders" onClick={Manejador}>Regístrate</button>
        </div>
    </div>
    </div>
  )
}

export default Login;